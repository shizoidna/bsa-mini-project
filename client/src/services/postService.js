import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request
  });
  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const reactPost = async (postId, reaction) => {
  const response = await callWebApi({
    endpoint: '/api/posts/react',
    type: 'PUT',
    request: {
      postId,
      reaction
    }
  });
  return response.json();
};

export const updatePost = async (postId, post, userId) => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}`,
    type: 'PUT',
    request: {
      userId,
      postId,
      post
    }
  });
  return response.json();
};

export const deletePost = async postId => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}`,
    type: 'DELETE',
    request: {
      postId
    }
  });
  return response.json();
};

// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);

import React, { useState } from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import styles from '../Comment/styles.module.scss';

const UpdatePost = ({
  post,
  updatePost
}) => {
  const [body, setBody] = useState(post.body);
  const handleUpdatedPost = async () => {
    if (!body) {
      return;
    }
    await updatePost(post.id, body);
    setBody(body);
  };
  return (
    <Segment className={styles.comment}>
      <Form onSubmit={handleUpdatedPost}>
        <Form.TextArea
          name="body"
          value={body}
          onChange={ev => setBody(ev.target.value)}
        />
        <Button floated="right" color="blue" type="submit">Post</Button>
      </Form>
    </Segment>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired
};

export default UpdatePost;

import React, { useState } from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import styles from '../Comment/styles.module.scss';

const UpdateComment = ({
  comment,
  updateComment
}) => {
  const [body, setBody] = useState(comment.body);
  const handleUpdatedComment = async () => {
    if (!body) {
      return;
    }
    await updateComment(comment.id, body);
    setBody(body);
  };

  return (
    <Segment className={styles.comment}>
      <Form onSubmit={handleUpdatedComment}>
        <Form.TextArea
          name="body"
          value={body}
          onChange={ev => setBody(ev.target.value)}
        />
        <Button floated="right" color="blue" type="submit">Save comment</Button>
      </Form>
    </Segment>
  );
};

UpdateComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired
};

export default UpdateComment;

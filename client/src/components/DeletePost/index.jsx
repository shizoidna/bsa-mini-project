import React from 'react';
import { Button, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import styles from '../Comment/styles.module.scss';

const DeletePost = ({
  toggleDeletedPost,
  deletePost,
  post
}) => (
  <Segment className={styles.comment}>
    <p>Are you sure you want to delete this post?</p>
    <Button onClick={() => toggleDeletedPost(null)}>No, I don`t want to delete this post.</Button>
    <Button onClick={() => deletePost(post, post.id)}>Yes, I want to delete this post.</Button>
  </Segment>
);

DeletePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  deletePost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired
};

export default DeletePost;

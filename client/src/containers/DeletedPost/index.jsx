import { Modal } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React from 'react';
import DeletePost from '../../components/DeletePost';
import { toggleDeletedPost, deletePost } from '../Thread/actions';
import Spinner from '../../components/Spinner';

const DeletedPost = ({
  post,
  deletePost: deleted,
  toggleDeletedPost: toggle
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <DeletePost
            toggleDeletedPost={toggle}
            deletePost={deleted}
            post={post}
          />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

DeletedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  deletePost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.deletedPost
});

const actions = { toggleDeletedPost, deletePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeletedPost);

import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UpdateComment from 'src/components/UpdateComment';
import { updateComment, toggleEditableComment } from 'src/containers/Thread/actions';
import Spinner from '../../components/Spinner';

const EditableComment = ({
  comment,
  updateComment: update,
  toggleEditableComment: toggle
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {comment
      ? (
        <Modal.Content>
          <UpdateComment
            comment={comment}
            updateComment={update}
          />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditableComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  toggleEditableComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  comment: rootState.posts.editableComment
});

const actions = { updateComment, toggleEditableComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditableComment);

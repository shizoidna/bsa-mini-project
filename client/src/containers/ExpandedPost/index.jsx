import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  reactPost,
  addComment,
  deletePost,
  toggleExpandedPost,
  toggleEditablePost,
  toggleDeletedPost,
  toggleEditableComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  me,
  toggleExpandedPost: toggleExpanded,
  toggleEditablePost: toggleEditable,
  toggleDeletedPost: toggleDeleted,
  toggleEditableComment: toggleEditableComm,
  addComment: add
}) => {
  const isMyPost = post.user.id === me.id;
  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggleExpanded()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              showEditIcon={isMyPost}
              reactPost={reactPost}
              toggleExpandedPost={toggleExpanded}
              toggleEditablePost={toggleEditable}
              toggleDeletedPost={toggleDeleted}
              sharePost={sharePost}
              deletePost={deletePost}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    isMyComment={comment.user.id === me.id}
                    toggleEditableComment={toggleEditableComm}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  me: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditablePost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired,
  toggleEditableComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  me: rootState.profile.user
});

const actions = {
  reactPost,
  toggleExpandedPost,
  toggleEditablePost,
  toggleDeletedPost,
  toggleEditableComment,
  addComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);

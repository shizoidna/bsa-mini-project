import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleEditablePost, updatePost } from 'src/containers/Thread/actions';
import UpdatePost from '../../components/UpdatePost';
import Spinner from '../../components/Spinner';

const EditablePost = ({
  post,
  updatePost: update,
  toggleEditablePost: toggle
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <UpdatePost
            updatePost={update}
            post={post}
          />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditablePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  toggleEditablePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editablePost
});

const actions = { toggleEditablePost, updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditablePost);

import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  UPDATE_COMMENT,
  SET_EXPANDED_POST,
  SET_EDITABLE_POST,
  SET_DELETED_POST,
  SET_EDITABLE_COMMENT
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case UPDATE_POST:
      return {
        ...state,
        posts: state.posts.map(post => (post.id === action.post.id ? action.post : post))
      };
    case UPDATE_COMMENT:
      return {
        ...state,
        posts: state.posts.map(post => (post.id === action.post.id ? action.post : post))
      };
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(post => post.id !== action.post.id)
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SET_EDITABLE_POST:
      return {
        ...state,
        editablePost: action.post
      };
    case SET_DELETED_POST:
      return {
        ...state,
        deletedPost: action.post
      };
    case SET_EDITABLE_COMMENT:
      return {
        ...state,
        editableComment: action.comment
      };
    default:
      return state;
  }
};

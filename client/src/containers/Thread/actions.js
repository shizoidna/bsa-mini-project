import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  UPDATE_COMMENT,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDITABLE_POST,
  SET_DELETED_POST,
  SET_EDITABLE_COMMENT
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

const deletePostAction = post => ({
  type: DELETE_POST,
  post
});

const updateCommentAction = post => ({
  type: UPDATE_COMMENT,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditablePostAction = post => ({
  type: SET_EDITABLE_POST,
  post
});

const setDeletedPostAction = post => ({
  type: SET_DELETED_POST,
  post
});

const setEditableCommentAction = comment => ({
  type: SET_EDITABLE_COMMENT,
  comment
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const toggleEditablePost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditablePostAction(post));
};

export const toggleDeletedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setDeletedPostAction(post));
};

export const toggleEditableComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setEditableCommentAction(comment));
};

export const reactPost = (postId, reaction) => async (dispatch, getRootState) => {
  const result = await postService.reactPost(postId, reaction);

  const mapLikes = post => ({
    ...post,
    likeCount: result.postReactions.like.toString(),
    dislikeCount: result.postReactions.dislike.toString()
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updatePost = (postId, post) => async dispatch => {
  const updatedPost = await postService.updatePost(postId, post, post.userId);
  dispatch(updatePostAction(updatedPost.newPost));
};

export const deletePost = (post, postId) => async dispatch => {
  await postService.deletePost(postId);
  dispatch(deletePostAction(post));
  dispatch(toggleDeletedPost(null));
};

export const updateComment = (commentId, comment) => async dispatch => {
  await commentService.updateComment(commentId, comment, comment.userId);
  const currentComment = await commentService.getComment(commentId);
  const post = await postService.getPost(currentComment.postId);
  dispatch(updateCommentAction(post));
  dispatch(setExpandedPostAction(post));
};

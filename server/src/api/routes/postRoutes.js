import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(result => {
      if (result.result === 'added' && result.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his post
        req.io.to(result.userId).emit(result.action, 'Your post got reaction!');
      }
      return res.send(result);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.editPost(req.user.id, req.body)
    .then(result => res.send(result))
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePost(req.user.id, req.body)
    .then(result => res.send(result))
    .catch(next));
export default router;

import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import { reactions } from '../../data/entities/reaction';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => {
  const post = postRepository.getPostById(id);
  // eslint-disable-next-line no-console
  post.then(result => console.log('POST SERVICE', result, id));
  return post;
};

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

const getPostReactions = async postId => {
  const postLikes = await postReactionRepository.getReactions(postId, 1);
  const postDislikes = await postReactionRepository.getReactions(postId, 2);

  return {
    like: postLikes.count,
    dislike: postDislikes.count
  };
};

export const setReaction = async (userId, { postId, reaction }) => {
  if (!userId || !postId) {
    throw new Error('Incorrect params');
  }

  if (!Object.keys(reactions).includes(reaction)) {
    throw new Error('This reaction doesn`t belong to type: reactions');
  }

  const pastReactions = await postReactionRepository.getPostReactions(userId, postId, reactions[reaction]);

  if (pastReactions.length > 0) {
    await postReactionRepository.clearPostReaction(userId, postId, [reactions[reaction]]);
    const postReactions = await getPostReactions(postId);
    return { userId, action: reactions[reaction], result: 'deleted', postReactions };
  }

  await postReactionRepository.clearPostReaction(userId, postId, [reactions.like, reactions.dislike]);
  await postReactionRepository.setPostReaction(userId, postId, reactions[reaction]);
  const postReactions = await getPostReactions(postId);
  return { userId, action: reactions[reaction], result: 'added', postReactions };
};

export const editPost = async (userId, { postId, post }) => {
  if (!userId || !postId) {
    throw new Error('Incorrect params');
  }

  await postRepository.updatePost(postId, post);
  const newPost = await postRepository.getPostById(postId);
  return { userId, result: 'updated', newPost };
};

export const deletePost = async (userId, { postId }) => {
  if (!userId || !postId) {
    throw new Error('Incorrect params');
  }

  await postRepository.deletePostById(postId);
  return { userId, result: 'deleted' };
};

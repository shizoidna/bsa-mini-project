import commentRepository from '../../data/repositories/commentRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const updateComment = async (userId, { commentId, commentText }) => {
  if (!userId || !commentId) {
    throw new Error('Incorrect params');
  }

  await commentRepository.updateComment(commentId, commentText);
  const newComment = await commentRepository.getCommentById(commentId);
  return { userId, result: 'updated', newComment };
};

export const deleteComment = async (userId, { commentId }) => {
  if (!userId || !commentId) {
    throw new Error('Incorrect params');
  }

  await commentRepository.deleteCommentById(commentId);
  return { userId, result: 'deleted' };
};

export const getCommentById = id => commentRepository.getCommentById(id);

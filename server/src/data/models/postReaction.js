export default (orm, DataTypes) => {
  const PostReaction = orm.define('postReaction', {
    action: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return PostReaction;
};

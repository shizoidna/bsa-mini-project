import { Op } from 'sequelize';
import { PostReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  /**
   * Get reaction
   * @param userId
   * @param postId
   * @param {number} action - 1 for like, 2 for dislike
   * @returns {*}
   */
  getPostReactions(userId, postId, action) {
    return this.model.findAll({
      where: { userId, postId, action }
    });
  }

  /**
   * Get reaction
   * @param postId
   * @param {number} action - 1 for like, 2 for dislike
   * @returns {*}
   */
  getReactions(postId, action) {
    return this.model.findAndCountAll({
      where: { postId, action }
    });
  }

  /**
   * Set reaction
   * @param userId
   * @param postId
   * @param {number} action - 1 for like, 2 for dislike
   * @returns {*}
   */
  setPostReaction(userId, postId, action) {
    return this.create({ userId, postId, action });
  }

  /**
   * Clear all reactions of certain type
   * @param userId
   * @param postId
   * @param {number[]} actions - 1 for like, 2 for dislike
   * @returns {*}
   */
  clearPostReaction(userId, postId, actions) {
    return this.model.destroy({
      where: {
        userId,
        postId,
        [Op.or]: actions.map(action => ({ action }))
      }
    });
  }
}

export default new PostReactionRepository(PostReactionModel);

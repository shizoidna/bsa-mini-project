import { CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: { id },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }]
    });
  }

  async updateComment(commentId, commentText) {
    return this.updateById(commentId, {
      body: commentText
    });
  }

  async deleteCommentById(commentId) {
    return this.updateById(commentId, {
      deletedAt: Date.now()
    });
  }
}

export default new CommentRepository(CommentModel);

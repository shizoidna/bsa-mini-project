import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import BaseRepository from './baseRepository';
import { reactions } from '../entities/reaction';

const actionCase = action => `CASE WHEN "postReactions"."action" = ${action} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      includeDeleted
    } = filter;

    const where = {};

    if (userId) {
      Object.assign(where, { userId });
    }

    if (!includeDeleted) {
      Object.assign(where, {
        deletedAt: null
      });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(actionCase(reactions.like))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(actionCase(reactions.dislike))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(actionCase(reactions.like))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(actionCase(reactions.dislike))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        where: { deletedAt: null },
        required: false,
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }

  async updatePost(id, post) {
    return this.updateById(id, {
      body: post
    });
  }

  async deletePostById(postId) {
    return this.updateById(postId, {
      deletedAt: Date.now()
    });
  }
}

export default new PostRepository(PostModel);

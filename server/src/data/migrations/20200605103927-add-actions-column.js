module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn(
        'postReactions',
        'action',
        {
          type: Sequelize.INTEGER,
          comment: '1 for LIKE, 2 for DISLIKE',
          allowNull: false,
          defaultValue: 1
        },
        {
          transaction
        }
      ),
      queryInterface.sequelize.query('DELETE FROM "postReactions" WHERE "isLike" = false', {
        transaction
      }),
      queryInterface.removeColumn('postReactions', 'isLike', { transaction }),
      queryInterface.changeColumn('postReactions', 'action', {
        type: Sequelize.INTEGER,
        comment: '1 for LIKE, 2 for DISLIKE',
        allowNull: false
      }, { transaction }),
      queryInterface.addColumn(
        'posts',
        'deletedAt',
        {
          type: Sequelize.DATE,
          allowNull: true
        },
        {
          transaction
        }
      ),
      queryInterface.addColumn(
        'comments',
        'deletedAt',
        {
          type: Sequelize.DATE,
          allowNull: true
        },
        {
          transaction
        }
      )
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('postReactions', 'isLike', {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }, { transaction }),
      queryInterface.removeColumn('postReactions', 'action', { transaction }),
      queryInterface.removeColumn('posts', 'deletedAt', { transaction }),
      queryInterface.removeColumn('comments', 'deletedAt', { transaction })
    ]))
};

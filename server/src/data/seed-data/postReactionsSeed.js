const now = new Date();

export default new Array(25)
  .fill(1)
  .map(action => ({
    action,
    createdAt: now,
    updatedAt: now
  }));
